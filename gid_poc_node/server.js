var fs = require('fs');
var express = require('express');
var app = express();
app.use(express.json()); // support json encoded bodies
app.use(express.urlencoded({ extended: true })); // support encoded bodies

var gid = require('attestation-agency-sdk');

var privateSigningKeyFilePath = './poc_Verification/key_for_signature_to_sign_successful_verifications/signing_key_for_globaliD.key';
var privateEncryptKeyFilePath = './poc_Verification/key_for_encrypting_data/signing_key_for_globaliD.key';
var signingPassphraseFilePath = './poc_Verification/key_for_signature_to_sign_successful_verifications/pass.txt';
var encryptPassphraseFilePath = './poc_Verification/key_for_encrypting_data/pass.txt';
var privateSigningKey = "";
var privateEncryptKey = "";
var signingPassphrase = "";
var encryptPassphrase = "";
var sdk = null;

(async() => {
    privateSigningKey = fs.readFileSync(privateSigningKeyFilePath, 'utf8');
    privateEncryptKey = fs.readFileSync(privateEncryptKeyFilePath, 'utf8');
    signingPassphrase = fs.readFileSync(signingPassphraseFilePath, 'utf8');
    encryptPassphrase = fs.readFileSync(encryptPassphraseFilePath, 'utf8');
    var config = {
        // client credentials
        clientId: '342f7da3-f03c-41ff-a9b0-cee233ccd730', // globalid api client id
        clientSecret: '1aeb703d3b2946048447927979094b1d', // globalid api client secret
        // signing and encryption key setup
        privateSigningKey: privateSigningKey,
        privateEncryptKey: privateEncryptKey,

        signingPassphrase: signingPassphrase,
        encryptPassphrase: encryptPassphrase,

        sandbox: false, // [default = false], if connecting to globaliD sandbox backend
    };

    sdk = await gid.default.init(config);
})();

app.get('/', function (req, res) {
    res.json({success: true, APIs: ["Verification", "ConfirmAttestation", "RevokeAttestation"]});
});

app.post('/Verification', async (req, res) => {
    try {
        var attestation_request_id = req.query.attestation_request_id;
        var enc_password = req.body.enc_password;
        var enc_data = req.body.enc_data;
        console.log("%s;%s;%s", attestation_request_id, enc_password, enc_data);

        var plainData = await sdk.verifyAndDecryptData(
            attestation_request_id,
            req.body
        );
        console.log('Address verification for ' + attestation_request_id + ': ', plainData);
        res.json(plainData);
    }
    catch (err) {
        console.error('Error processing Verification for ' + attestation_request_id + '. ', err);
        res.status(500).send(err.toString());
    }
});

app.post('/ConfirmAttestation', async (req, res) => {
    try {
        var attestation_request_id = req.query.attestation_request_id;
        var address = req.body.address;
        if (address != undefined && address) {
            var pii = { address: address };
            await sdk.confirmAttestationRequest(attestation_request_id, pii);
            console.log('attestation ' + attestation_request_id + ' is confirmed');
            res.json({ attestation: "confirmed" });
        } else {
            console.error('error processing ConfirmAttestation for ' + attestation_request_id + '. ', err);
            res.status(400).send('Missing address');
        }
    }
    catch (err) {
        console.error('Error processing ConfirmAttestation for ' + attestation_request_id + '. ', err);
        res.status(500).send(err.toString());
    }
});

app.post('/RejectAttestation', async (req, res) => {
    try {
        var attestation_request_id = req.query.attestation_request_id;
        await sdk.failAttestationRequestWithCode(attestation_request_id, "600-1");
            console.log('attestation ' + attestation_request_id + ' is rejected');
            res.json({ attestation: "rejected" });
        }
    catch (err) {
        console.error('Error processing RejectAttestation for ' + attestation_request_id + '. ', err);
        res.status(500).send(err.toString());
    }
});

app.post('/RevokeAttestation', async (req, res) => {
    try {
        var attestation_request_id = req.query.attestation_request_id;
        await sdk.revokeAttestationRequest(attestation_request_id, attestation_request_id);
        console.log('attestation ' + attestation_request_id + ' is revoked');
        res.json({ attestation: "revoked" });
    }
    catch (err) {
        console.error('Error processing RevokeAttestation for ' + attestation_request_id + '. ', err);
        res.status(500).send(err.toString());
    }
});

var server = app.listen(8081, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log("gid_poc_node listening at http://%s:%s", host, port);
});