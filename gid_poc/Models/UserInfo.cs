﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gid_poc.Models
{
    public class UserInfo
    {
        public string Id { get; set; }
        public string AccessToken { get; set; }
        public string IdToken { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string ProfileImgUri { get; set; }

        public string GetName() => string.IsNullOrEmpty(DisplayName) ? Name : DisplayName;
    }
}
