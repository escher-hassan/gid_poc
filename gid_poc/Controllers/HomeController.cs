﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using gid_poc.Models;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Configuration;
using System.IdentityModel.Tokens.Jwt;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Crypto.Engines;
using System.Text;
using Org.BouncyCastle.Crypto.Encodings;
using System.Security.Cryptography;
using System.IO;
using Org.BouncyCastle.Asn1.Cms.Ecc;
using Microsoft.AspNetCore.SignalR;

namespace gid_poc.Controllers
{
    public class AddressVerification
    {
        public string TerminalId { get; set; }
        public string AttestationRequestId { get; set; }
        public string Address { get; set; }
    }

    public class HomeController : Controller
    {
        enum GidApis { PublicQrCode, OpenIdQrCode, AuthToken, Identity, Images, GetEncryptedData };
        private readonly IConfiguration _config;
        private readonly ILogger<HomeController> _logger;
        private readonly IHubContext<GidHub> _hubcontext;

	private static readonly IDictionary<string, AddressVerification> _addressVerifications = new Dictionary<string, AddressVerification>();
        private readonly AttestationManager _attestationManager;

        public HomeController(IConfiguration config, ILogger<HomeController> logger, IHubContext<GidHub> hubcontext)
        {
            _config = config;
            _logger = logger;
            _hubcontext = hubcontext;
            _attestationManager = new AttestationManager();
        }

        public async Task<IActionResult> Index(string code)
        {
            var userInfo = await GetPublicUserInfo(await GetUserAccessToken(code));
            if (userInfo != null)
            {
                ViewData["Name"] = userInfo.GetName();
                ViewData["ProfileImgUri"] = userInfo.ProfileImgUri;
                ViewData["PrivateData"] = await GetPrivateData(userInfo.IdToken);
            }
            ViewData["QrCodeUri"] = GetUrl(GidApis.OpenIdQrCode);
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult ToU()
        {
            return View();
        }

        public IActionResult Verifications()
        {
            return View();
        }



        public class EncData
        {
            public string enc_password { get; set; }
            public string enc_data { get; set; }
        }

        [HttpPost]
        public async Task Verification(string attestation_request_id, [FromBody] EncData data)
        {
            var client = new HttpClient();
            var jData = JsonConvert.SerializeObject(data);
            var content = new StringContent(jData, Encoding.UTF8, "application/json");
            var res = await client.PostAsync($"http://localhost:8081/Verification?attestation_request_id={attestation_request_id}", content);
            if (res.IsSuccessStatusCode)
            {
                var str = await res.Content.ReadAsStringAsync();
                var json = JsonConvert.DeserializeObject(str) as JObject;
                var address = json["address"]?.ToString();
                var terminal_id = json["terminal_id"]?.ToString();
                await _hubcontext.Clients.All.SendCoreAsync("VerificationRequest", new string[] { attestation_request_id, address, terminal_id });
                _addressVerifications[terminal_id] = new AddressVerification { TerminalId = terminal_id, AttestationRequestId = attestation_request_id, Address = address };
            }
        }

        [HttpGet]
        public AddressVerification GetAddressVerification(string terminal_id)
        {
            if (_addressVerifications.TryGetValue(terminal_id, out AddressVerification addressVerification))
            {
                _addressVerifications.Remove(terminal_id);
                return addressVerification;
            }
            return null;
        }

        private string GetUrl(GidApis api)
        {
            string apiUrl = "";
            var clientId = _config["GlobaliDSettings:ClientId"];
            var redirectUri = _config["GlobaliDSettings:RedirectUri"];
            var acrcId = _config["GlobaliDSettings:AcrcId"];
            switch (api)
            {
                case GidApis.PublicQrCode:
                    apiUrl = $"https://auth.global.id/?client_id={clientId}&response_type=code&scope=public&redirect_uri={redirectUri}";
                    break;
                case GidApis.OpenIdQrCode:
                    Random random = new Random();
                    const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                    var nonce = new string(Enumerable.Repeat(chars, 8).Select(s => s[random.Next(s.Length)]).ToArray());
                    apiUrl = $"https://auth.global.id/?client_id={clientId}&response_type=code&scope=openid&redirect_uri={redirectUri}&acrc_id={acrcId}&nonce={nonce}";
                    break;
                case GidApis.AuthToken:
                    apiUrl = "https://api.global.id/v1/auth/token";
                    break;
                case GidApis.Identity:
                    apiUrl = "https://api.global.id/v1/identities/me";
                    break;
                case GidApis.Images:
                    apiUrl = "https://api.global.id/v1/identities/{0}/images";
                    break;
                case GidApis.GetEncryptedData:
                    apiUrl = "https://api.globalid.net/v1/vault/get-encrypted-data";
                    break;
            }
            return apiUrl;
        }

        private async Task<string> GetAccessTokenUsingPublicCode(string code)
        {
            string accessToken = null;
            if (!string.IsNullOrEmpty(code))
            {
                var client = new HttpClient();
                var values = new Dictionary<string, string>
                {
                    {"client_id", _config["GlobaliDSettings:ClientId"]},
                    {"client_secret", _config["GlobaliDSettings:ClientSecret"]},
                    {"code", code},
                    {"redirect_uri", _config["GlobaliDSettings:RedirectUri"]},
                    {"grant_type", "authorization_code"},
                };
                var content = new FormUrlEncodedContent(values);
                var res = await client.PostAsync(GetUrl(GidApis.AuthToken), content);
                if (res.IsSuccessStatusCode)
                {
                    var str = await res.Content.ReadAsStringAsync();
                    var json = JsonConvert.DeserializeObject(str) as JObject;
                    accessToken = json["access_token"]?.ToString();
                }
            }
            return accessToken;
        }

        private async Task<UserInfo> GetUserAccessToken(string code)
        {
            UserInfo userInfo = null;
            if (!string.IsNullOrEmpty(code))
            {
                var client = new HttpClient();
                var values = new Dictionary<string, string>
                {
                    {"client_id", _config["GlobaliDSettings:ClientId"]},
                    {"client_secret", _config["GlobaliDSettings:ClientSecret"]},
                    {"code", code},
                    {"redirect_uri", _config["GlobaliDSettings:RedirectUri"]},
                    {"grant_type", "authorization_code"},
                };
                var content = new FormUrlEncodedContent(values);
                var res = await client.PostAsync(GetUrl(GidApis.AuthToken), content);
                if (res.IsSuccessStatusCode)
                {
                    var str = await res.Content.ReadAsStringAsync();
                    var json = JsonConvert.DeserializeObject(str) as JObject;

                    userInfo = new UserInfo
                    {
                        AccessToken = json["access_token"]?.ToString(),
                        IdToken = json["id_token"]?.ToString(),
                    };
                }
            }
            return userInfo;
        }

        private async Task<string> GetAppAccessToken()
        {
            string appAccessToken = null;
            var client = new HttpClient();
            var values = new Dictionary<string, string>
                {
                    {"client_id", _config["GlobaliDSettings:ClientId"]},
                    {"client_secret", _config["GlobaliDSettings:ClientSecret"]},
                    {"grant_type", "client_credentials"},
                };
            var content = new FormUrlEncodedContent(values);
            var res = await client.PostAsync(GetUrl(GidApis.AuthToken), content);
            if (res.IsSuccessStatusCode)
            {
                var str = await res.Content.ReadAsStringAsync();
                var json = JsonConvert.DeserializeObject(str) as JObject;

                appAccessToken = json["access_token"]?.ToString();
            }
            return appAccessToken;
        }

        private async Task<IEnumerable<string>> GetPrivateData(string idToken)
        {
            var handler = new JwtSecurityTokenHandler();
            var token = handler.ReadJwtToken(idToken);
            var acrcId = _config["GlobaliDSettings:AcrcId"];
            var acrc = token.Claims.Where(c => c.Type == $"idp.globalid.net/claims/{acrcId}").FirstOrDefault();
            if (!string.IsNullOrEmpty(acrc?.Value))
            {
                var acrcJson = JObject.Parse(acrc.Value);
                var encrypted_data_tokens = acrcJson.Properties().SelectMany(p => p.Value.ToObject<string[]>());
                var data_tokens = DecryptTokens(encrypted_data_tokens);

                string appAccessToken = await GetAppAccessToken();

                var client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", appAccessToken);
                var obj = new { private_data_tokens = data_tokens };
                var private_data_tokens = JsonConvert.SerializeObject(obj);
                private_data_tokens = private_data_tokens.Replace("\\", "");
                var content = new StringContent(private_data_tokens, Encoding.UTF8, "application/json");
                var res = await client.PostAsync(GetUrl(GidApis.GetEncryptedData), content);
                if (res.IsSuccessStatusCode)
                {
                    var str = await res.Content.ReadAsStringAsync();
                    List<GidEncryptedData> encryptedDataList = JsonConvert.DeserializeObject<List<GidEncryptedData>>(str);
                    return DecryptPrivateData(encryptedDataList);
                }

            }
            return null;
        }

        private static byte[] HexToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        private IEnumerable<string> DecryptPrivateData(List<GidEncryptedData> encryptedDataList)
        {
            var privateKeyPath = _config["GlobaliDSettings:PrivateKeyPath"];
            var privateKeyPassword = _config["GlobaliDSettings:PrivateKeyPassword"];
            List<string> data_tokens = new List<string>();
            try
            {
                using (var reader = System.IO.File.OpenText(privateKeyPath))
                {
                    AsymmetricCipherKeyPair keyPair = (AsymmetricCipherKeyPair)new PemReader(reader, new PasswordFinder(privateKeyPassword)).ReadObject();
                    var decryptEngine = new OaepEncoding(new RsaEngine());
                    decryptEngine.Init(false, keyPair.Private);

                    foreach (var encryptedData in encryptedDataList)
                    {
                        try
                        {
                            var passwdbytesToDecrypt = Convert.FromBase64String(encryptedData.encrypted_data_password);
                            var dataBytesToDecrypt = Convert.FromBase64String(encryptedData.GetEncryptedData());
                            var passwd = HexToByteArray(Encoding.UTF8.GetString(decryptEngine.ProcessBlock(passwdbytesToDecrypt, 0, passwdbytesToDecrypt.Length)));
                            using (AesCryptoServiceProvider myAes = new AesCryptoServiceProvider())
                            {
                                // Encrypt the string to an array of bytes.

                                // Decrypt the bytes to a string.
                                myAes.CreateDecryptor();
                                myAes.Mode = CipherMode.CBC;
                                myAes.KeySize = 256;
                                myAes.Key = passwd;
                                var iv = HexToByteArray(encryptedData.GetIV());
                                myAes.IV = iv;
                                var decryptor = myAes.CreateDecryptor(myAes.Key, myAes.IV);
                                using (MemoryStream msDecrypt = new MemoryStream(dataBytesToDecrypt))
                                {
                                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                                    {
                                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                                        {

                                            // Read the decrypted bytes from the decrypting stream
                                            // and place them in a string.
                                            data_tokens.Add(srDecrypt.ReadToEnd());
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            //TODO display error
                        }
                    }
                }
            }
            catch (Exception e)
            {
                //TODO Display error
            }

            return data_tokens;
        }

        private IEnumerable<string> DecryptTokens(IEnumerable<string> base64EncryptedTokens)
        {
            var privateKeyPath = _config["GlobaliDSettings:PrivateKeyPath"];
            var privateKeyPassword = _config["GlobaliDSettings:PrivateKeyPassword"];
            List<string> data_tokens = new List<string>();
            try
            {
                using (var reader = System.IO.File.OpenText(privateKeyPath))
                {
                    AsymmetricCipherKeyPair keyPair = (AsymmetricCipherKeyPair)new PemReader(reader, new PasswordFinder(privateKeyPassword)).ReadObject();
                    var decryptEngine = new OaepEncoding(new RsaEngine());
                    decryptEngine.Init(false, keyPair.Private);

                    foreach (var base64EncryptedToken in base64EncryptedTokens)
                    {
                        try
                        {
                            var bytesToDecrypt = Convert.FromBase64String(base64EncryptedToken);
                            var decrypted = Encoding.UTF8.GetString(decryptEngine.ProcessBlock(bytesToDecrypt, 0, bytesToDecrypt.Length));
                            data_tokens.Add(decrypted);
                        }
                        catch (Exception e)
                        {
                            //TODO display error
                        }
                    }
                }
            }
            catch (Exception e)
            {
                //TODO Display error
            }

            return data_tokens;
        }

        //private async Task<string[]> GetImgs(string accessToken, string uid)
        //{

        //    var imageUrl = string.Format(GetUrl(GidApis.Images), userInfo.Id);
        //    res = await client.GetAsync(imageUrl);
        //    if (res.IsSuccessStatusCode)
        //    {
        //        str = await res.Content.ReadAsStringAsync();
        //        json = JsonConvert.DeserializeObject(str) as JObject;
        //        var imgs = json["data"].ToArray();
        //        if (imgs.Length > 0)
        //        {
        //            var profileImg = imgs.FirstOrDefault();
        //            if (profileImg != null)
        //            {
        //                userInfo.ProfileImgUri = profileImg["url"].ToString();
        //            }
        //        }
        //    }

        //    return null;
        //}

        private async Task<UserInfo> GetPublicUserInfo(UserInfo userInfo)
        {
            if (!string.IsNullOrEmpty(userInfo?.AccessToken))
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userInfo.AccessToken);
                var res = await client.GetAsync(GetUrl(GidApis.Identity));
                if (res.IsSuccessStatusCode)
                {
                    var str = await res.Content.ReadAsStringAsync();
                    var json = JsonConvert.DeserializeObject(str) as JObject;
                    userInfo.Id = json["gid_uuid"]?.ToString();
                    userInfo.Name = json["gid_name"]?.ToString();
                    userInfo.DisplayName = json["display_name"]?.ToString();
                    userInfo.ProfileImgUri = json["display_image_url"]?.ToString();
                    return userInfo;
                }
            }
            return null;
        }

        [HttpPost]
        public async Task<HttpResponseMessage> ConfirmAttestation(string attestation_request_id, [FromBody] AddressVerification address)
        {
            return await _attestationManager.ConfirmAttestation(attestation_request_id, address.Address);
        }


        private class GidEncryptedData
        {
            public string encrypted_data { get; set; }
            public string encrypted_data_password { get; set; }
            public string encrypted_root_password { get; set; }
            public string token { get; set; }
            public string GetEncryptedData() => encrypted_data.Substring(32);
            public string GetIV() => encrypted_data.Substring(0, 32);
        }

        private class PasswordFinder : IPasswordFinder
        {
            private string password;

            public PasswordFinder(string password)
            {
                this.password = password;
            }

            public char[] GetPassword()
            {
                return password.ToCharArray();
            }
        }
    }

    public class AttestationManager
    {
        public async Task<HttpResponseMessage> ConfirmAttestation(string attestation_request_id, string address)
        {
            var client = new HttpClient();
            var jData = $"{{\"address\":\"{address}\"}}";
            var content = new StringContent(jData, Encoding.UTF8, "application/json");
            var res = await client.PostAsync($"http://localhost:8081/ConfirmAttestation?attestation_request_id={attestation_request_id}", content);
            return res;
        }

        public async Task<HttpResponseMessage> RejectAttestation(string attestation_request_id)
        {
            var client = new HttpClient();
            var jData = $"{{}}";
            var content = new StringContent(jData, Encoding.UTF8, "application/json");
            var res = await client.PostAsync($"http://localhost:8081/RejectAttestation?attestation_request_id={attestation_request_id}", content);
            return res;
        }

        public async Task<HttpResponseMessage> RevokeAttestation(string attestation_request_id)
        {
            var client = new HttpClient();
            var jData = $"{{}}";
            var content = new StringContent(jData, Encoding.UTF8, "application/json");
            var res = await client.PostAsync($"http://localhost:8081/RevokeAttestation?attestation_request_id={attestation_request_id}", content);
            return res;
        }
    }

    public class GidHub : Hub
    {
        private readonly AttestationManager _attestationManager;

        public GidHub()
        {
            _attestationManager = new AttestationManager();
        }

        public async Task ConfirmAttestation(string attestation_request_id, string address)
        {
            var res = await _attestationManager.ConfirmAttestation(attestation_request_id, address);
            if (res.IsSuccessStatusCode)
            {
                await this.Clients.All.SendCoreAsync("AttestationConfirmed", new string[] { attestation_request_id });
            }
            else
            {
                await this.Clients.All.SendCoreAsync("AttestationNotConfirmed", new string[] { attestation_request_id });
            }
        }

        public async Task RejectAttestation(string attestation_request_id)
        {
            var res = await _attestationManager.RejectAttestation(attestation_request_id);
            if (res.IsSuccessStatusCode)
            {
                await this.Clients.All.SendCoreAsync("AttestationRejected", new string[] { attestation_request_id });
            }
            else
            {
                await this.Clients.All.SendCoreAsync("AttestationNotRejected", new string[] { attestation_request_id });
            }
        }

        public async Task RevokeAttestation(string attestation_request_id)
        {
            var res = await _attestationManager.RevokeAttestation(attestation_request_id);
            if (res.IsSuccessStatusCode)
            {
                await this.Clients.All.SendCoreAsync("AttestationRevoked", new string[] { attestation_request_id });
            }
            else
            {
                await this.Clients.All.SendCoreAsync("AttestationNotRevoked", new string[] { attestation_request_id });
            }
        }
    }
}
